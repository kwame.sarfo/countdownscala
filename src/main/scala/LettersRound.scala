import scala.annotation.tailrec
import scala.collection.mutable
import scala.util.Random

class LettersRound (val c1:Contestant, val c2:Contestant){

  def selectLetters() = {
    val player1Letters = populate()
    val player2Letters = populate()
  }

  def populate(): List[Char] = {
    @tailrec
    def getLetter(acc: List[Char], vowelCount: Int, consonantCount: Int): List[Char] = {
      if (vowelCount >= 3 && consonantCount >= 4 && acc.length == 9) acc
      else {
        Random.between(1, 2) match {
          case 1 => {
            getLetter(acc :+ LettersRound.getVowelStack().head, vowelCount + 1, consonantCount)
          }
          case 2 => {
            getLetter(acc :+ LettersRound.getConsonantStack().head, vowelCount, consonantCount + 1)
          }
        }
      }
    }

    getLetter(List(), 0, 0)
  }

}

object LettersRound {
  def getConsonantStack() = {
   List[Char]("b", "c", "m", "d" , "f", "g", "h", "j", "k",
    "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z")
  }

  def getVowelStack() = {
    List[Char]("a", "e", "i", "o", "u")
  }
}
